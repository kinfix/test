<?php
/**
 * Created by PhpStorm.
 * User: Kostadin
 * Date: 09.02.2018
 * Time: 12:06
 */

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ImageUploadController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function imageUpload()

    {

        return view('layouts.imageUpload');

    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function imageUploadPost()

    {

        request()->validate([

            'image' => 'required|image|mimes:jpeg,png,jpg,gif|max:2048',

        ]);


        $imageName = time() . '.' . request()->image->getClientOriginalExtension();

        request()->image->store('images');


        return back()
            ->with('success', 'You have successfully upload image.')
            ->with('image', $imageName);

    }
}