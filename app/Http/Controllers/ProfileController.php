<?php

namespace App\Http\Controllers;

use App\Order;


class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function sales(Order $order)
    {
        $sales = Order::sales();

        return view('profile.sales', compact('sales'));
    }

    public function purchases()
    {
        $purchases = Order::purchases();

        return view('profile.purchases', compact('purchases'));
    }

    public function info()
    {
        return view('profile.info');
    }

}
