<?php

namespace App\Http\Controllers;

use App\Order;
use App\Product;
use Illuminate\Support\Facades\Auth;


class OrdersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function store(Product $product)
    {
        Order::create([
            'product_id' => $product->id,
            'price' => $product->price,
            'seller_id' => $product->user_id,
            'buyer_id' => Auth::id()
        ]);

        $product->quantity -= 1;
        $product->save();

       return redirect()->back()->with('message', 'Спасибо за покупку!');
    }
}
