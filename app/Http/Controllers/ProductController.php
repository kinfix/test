<?php

namespace App\Http\Controllers;

use App\Product;
use function compact;
use function dd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use function view;


class ProductController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function create()
    {
        return view('main.new-product');
    }

    public function store()
    {
        $formInput = request()->except('image');

        Product::validation();


            $imagePath = request()->image->store('images');

            $formInput['image'] = $imagePath;



        Product::create($formInput);

        return redirect('/');
    }


    public function showEditor(Product $product)
    {
        return view('main.edit-product', compact('product'));
    }

    public function edit(Product $product)
    {
        $formInput = request()->except('image');

        Product::validation();

        if(\request()->image != null) {

            Storage::delete($product->image);

            $imagePath = request()->image->store('images');

            $formInput['image'] = $imagePath;

        }

        $product->update($formInput);

        return redirect('product/' . $product->id)->with('message', 'Данные изменены');
    }

}
