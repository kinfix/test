<?php

namespace App\Http\Controllers;

use App\Product;

use App\User;
use function dd;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function index(Product $product)
    {
        $products = $product->byDate();

        if ($filter = \request()->get('filter')) {
            switch ($filter) {
                case 'title':
                    $products = $product->byName();
                    break;
                case 'date':
                    $products = $product->byDate();
                    break;
                case 'price':
                    $products = $product->byPrice();
                    break;
            }
        }

        return view('main.products', compact('products'));

    }

    public function show(Product $product)
    {
        $sellerInfo = DB::table('users')->where('id', '=', $product->user_id)->get();

        return view('main.product-page', compact('product', 'sellerInfo'));
    }

}
