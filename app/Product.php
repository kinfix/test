<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\DB;

class Product extends Model
{

    protected $fillable = ['title', 'description', 'quantity', 'price', 'image', 'user_id'];

    public function byDate()
    {
        return static::where('quantity', '>', '0')->orderBy('created_at', 'desc')->get();
    }

    public function byPrice()
    {
        return static::where('quantity', '>', '0')->orderBy('price', 'asc')->get();
    }

    public function byName()
    {
        return static::where('quantity', '>', '0')->orderBy('title', 'asc')->get();
    }

    public function user()
    {
        $this->belongsTo(User::class);
    }

    public function order()
    {
        $this->hasMany(Order::class);
    }

    public static function validation()
    {
        return request()->validate([

            'title' => 'required|string|max:100',

            'description' => 'required|string|max:64000',

            'price' => 'required|numeric',

            'quantity' => 'required|numeric|max:100',

            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048',

        ]);
    }
}
