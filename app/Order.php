<?php

namespace App;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;


class Order extends Model
{
    protected $fillable = ['product_id', 'price', 'seller_id', 'buyer_id',];

    public function user()
    {
        $this->belongsTo(User::class);
    }

    public function product()
    {
        $this->belongsTo(Product::class);
    }

    /*
    * Show the logged user's sales
    */
    public static function sales()
    {
        return $sales = DB::select('SELECT
            `products`.`title`, `products` . `price`,
            COUNT(`orders` . `product_id`) AS `quantity`,
            MAX(`orders` . `created_at`) AS `latest_order`,
                `users` . `name`,
                `users` . `email`,
                `users` . `phone_number`
            FROM
                `orders`
            INNER JOIN `products` ON `products` . `id` = `orders` . `product_id`
            INNER JOIN `users` ON `users` . `id` = `orders` . `buyer_id`
            WHERE
                `seller_id` = ?
            GROUP BY
                `orders` . `product_id`, `users`.`name`, `users`.`email`', [Auth::id()]);
    }

    public static function purchases()
    {
        return $purchases = DB::select('SELECT
            `products`.`title`, `products` . `price`,
            COUNT(`orders` . `product_id`) AS `quantity`,
            MAX(`orders` . `created_at`) AS `latest_order`,
                `users` . `name`,
                `users` . `email`,
                `users` . `phone_number`
            FROM
                `orders`
            INNER JOIN `products` ON `products` . `id` = `orders` . `product_id`
            INNER JOIN `users` ON `users` . `id` = `products` . `user_id`
            WHERE
                `buyer_id` = ?
            GROUP BY
                `orders` . `product_id`', [Auth::id()]);
    }
}
