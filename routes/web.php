<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'HomeController@index');

Route::get('/new-product', 'ProductController@create');
Route::get('/product/{product}', 'HomeController@show');
Route::get('/editor/{product}', 'ProductController@showEditor');
Route::post('/edit/{product}', 'ProductController@edit');
Route::post('/store-product', 'ProductController@store');

Route::get('/sales', 'ProfileController@sales');
Route::get('/purchases', 'ProfileController@purchases');

Route::get('/order/{product}', 'OrdersController@store');