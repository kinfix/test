-- --------------------------------------------------------
-- Хост:                         127.0.0.1
-- Версия сервера:               5.7.19 - MySQL Community Server (GPL)
-- Операционная система:         Win64
-- HeidiSQL Версия:              9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Дамп структуры базы данных test
CREATE DATABASE IF NOT EXISTS `test` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `test`;

-- Дамп структуры для таблица test.migrations
CREATE TABLE IF NOT EXISTS `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы test.migrations: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
	(1, '2014_10_12_000000_create_users_table', 1),
	(2, '2014_10_12_100000_create_password_resets_table', 1),
	(3, '2018_02_08_170034_create_products_table', 1),
	(4, '2018_02_11_085144_create_orders_table', 1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;

-- Дамп структуры для таблица test.orders
CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `product_id` int(10) unsigned NOT NULL,
  `price` double(5,2) NOT NULL,
  `seller_id` int(10) unsigned NOT NULL,
  `buyer_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `orders_product_id_foreign` (`product_id`),
  KEY `orders_seller_id_foreign` (`seller_id`),
  KEY `orders_buyer_id_foreign` (`buyer_id`),
  CONSTRAINT `orders_buyer_id_foreign` FOREIGN KEY (`buyer_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `orders_product_id_foreign` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `orders_seller_id_foreign` FOREIGN KEY (`seller_id`) REFERENCES `users` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы test.orders: ~17 rows (приблизительно)
/*!40000 ALTER TABLE `orders` DISABLE KEYS */;
INSERT INTO `orders` (`id`, `product_id`, `price`, `seller_id`, `buyer_id`, `created_at`, `updated_at`) VALUES
	(1, 2, 568.54, 1, 2, '2018-02-13 14:29:11', '2018-02-13 14:29:11'),
	(2, 2, 568.54, 1, 2, '2018-02-13 14:29:14', '2018-02-13 14:29:14'),
	(3, 1, 56.37, 1, 2, '2018-02-13 15:05:36', '2018-02-13 15:05:36'),
	(4, 1, 56.37, 1, 2, '2018-02-13 16:05:22', '2018-02-13 16:05:22'),
	(5, 1, 56.37, 1, 2, '2018-02-13 16:05:23', '2018-02-13 16:05:23'),
	(6, 5, 54.85, 2, 3, '2018-02-13 16:29:47', '2018-02-13 16:29:47'),
	(7, 2, 568.54, 1, 3, '2018-02-13 16:29:50', '2018-02-13 16:29:50'),
	(8, 1, 56.37, 1, 3, '2018-02-13 16:29:54', '2018-02-13 16:29:54'),
	(9, 1, 56.37, 1, 3, '2018-02-13 16:29:54', '2018-02-13 16:29:54'),
	(10, 4, 145.00, 3, 4, '2018-02-13 17:53:38', '2018-02-13 17:53:38'),
	(11, 5, 54.85, 2, 4, '2018-02-13 17:53:41', '2018-02-13 17:53:41'),
	(12, 5, 54.85, 2, 4, '2018-02-13 17:53:43', '2018-02-13 17:53:43'),
	(13, 2, 568.54, 1, 4, '2018-02-13 17:53:46', '2018-02-13 17:53:46'),
	(14, 4, 145.00, 3, 1, '2018-02-13 17:59:04', '2018-02-13 17:59:04'),
	(15, 4, 145.00, 3, 1, '2018-02-13 17:59:04', '2018-02-13 17:59:04'),
	(16, 5, 54.85, 2, 1, '2018-02-14 09:19:38', '2018-02-14 09:19:38'),
	(17, 4, 145.00, 3, 1, '2018-02-14 09:37:28', '2018-02-14 09:37:28');
/*!40000 ALTER TABLE `orders` ENABLE KEYS */;

-- Дамп структуры для таблица test.password_resets
CREATE TABLE IF NOT EXISTS `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- Дамп данных таблицы test.password_resets: ~0 rows (приблизительно)
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;

-- Дамп структуры для таблица test.products
CREATE TABLE IF NOT EXISTS `products` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci NOT NULL,
  `quantity` tinyint(4) NOT NULL,
  `price` double(7,2) NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `products_user_id_foreign` (`user_id`),
  CONSTRAINT `products_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы test.products: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `products` DISABLE KEYS */;
INSERT INTO `products` (`id`, `title`, `description`, `quantity`, `price`, `image`, `user_id`, `created_at`, `updated_at`) VALUES
	(1, 'Зажигалочкa', 'Качественная зажигалка. Может зажигать, а может не зажигать.', 63, 56.37, 'images/6FFJIMJq6MUjfs2ge4MsecroIZ3tpGwNL194F3dT.gif', 1, '2018-02-13 14:23:07', '2018-02-15 07:15:20'),
	(2, 'Телега', 'Хорошая телега. Максимальная комплектация, полный привод. Без пробега по России.', 4, 568.54, 'images/9y5xrsJE7o8eIVvpRgx8YiWFqfWMcVD43dhK3JYs.jpeg', 1, '2018-02-13 14:24:03', '2018-02-15 07:15:10'),
	(4, 'Носки', 'Моя бабушка сделала', 52, 145.00, 'images/85jFxdgyMNGzjvUDuxJeSds5aqbGkxFdXVW9dOWD.jpeg', 3, '2018-02-13 16:29:39', '2018-02-15 07:16:21'),
	(5, 'Колесо', 'Слегка потёрто, а так - в идеале. Дорогу отлично держит.', 4, 54.85, 'images/HriEtqfkfJjMpygTx0cLYOZABFXDvjYnPMc1SmrK.jpeg', 2, '2018-02-13 14:25:22', '2018-02-15 07:16:01');
/*!40000 ALTER TABLE `products` ENABLE KEYS */;

-- Дамп структуры для таблица test.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone_number` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Дамп данных таблицы test.users: ~4 rows (приблизительно)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone_number`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'Константин', '1@1.com', '$2y$10$xMgSACWB0i.1GScKvGgJmOoqG1/yV/0u6tF/hto1ePyjFs7pImWZu', '+375295555555', 'iQmlwKOEAjWu8zGGDHYEsFN5GGnTyLDl6XvyvBuM3yghrppi7bhCArvfjsbh', '2018-02-13 14:21:42', '2018-02-13 14:21:42'),
	(2, 'Алесандр', '2@2.com', '$2y$10$F9RIUJisaqvR/QiL1pnAFOtCxuhg6McCy3ktRjWwnmYmvQ0Z5BKHS', '80292222222', 'VWc2EP6stYX8OCCBptFp4EEnKqBgwpuVVxR1c70xOTRDBSOINHU2i8UQcKrr', '2018-02-13 14:24:35', '2018-02-13 14:24:35'),
	(3, 'Игорь', '3@3.com', '$2y$10$OIzx0rJu1GC2b5nnRW7aN.KXidAiNKXsPvUJCnYZ0TJT4ADgMbuIS', '+375295656633', 'aFFALRS15ouzVcAgX1mYd4eNKE5I5BwKJSugD0QVbuxGukKeF8lAxt65NaUx', '2018-02-13 16:29:17', '2018-02-13 16:29:17'),
	(4, 'Всеволод', '4@4.com', '$2y$10$80DJf5PYkCcrloMaOEM5XuXbNSZEj.tBJvX15/TYUw/LRmSLQ/a0i', '+375295554422', 'iaxvXbjE2JipK9knAlGnnxNmqUSRTsP36ixVb8CZBZdUuuhR6WenYqxW9OsS', '2018-02-13 17:53:25', '2018-02-13 17:53:25');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
