Laravel Test

## Instuctions 

Requirements: PHP 7.1+, MySQL 5.7+

- Download the repository to your webserver folder or wherever you'd like; 
- Run 'composer install';
- Use 'test.sql' file that in the root of the project in order to setup the database, table and test-data;
- If you need an empty table-structure: create a new database, fill in 'env.' file with DB-credentials(in the root of the project) then run 'php artisan migrate' ;
- Run 'composer dump-autoload' and 'php artisan config:cache';
- Start your web-server. 
  If you don't use one, run 'php artisan serve' and go to 'localhost:8000' in your browser; 
  
You're all set 


## License

The Laravel framework is open-sourced software licensed under the [MIT license](https://opensource.org/licenses/MIT).
