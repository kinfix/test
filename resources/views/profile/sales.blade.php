@extends('layouts.base')

@section('content')

    <div class="container mt-5">

        <h1 class="text-center">Продажи</h1>

        <div class="col-12 mt-5">

            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Название</th>
                    <th scope="col">Цена(руб.)</th>
                    <th scope="col">Количество</th>
                    <th scope="col">Последняя продажа</th>
                    <th scope="col">Покупатель</th>
                    <th scope="col">Телефон</th>
                    <th scope="col">Email</th>

                </tr>
                </thead>
                <tbody>
                @foreach($sales as $sale)
                    <tr>
                        <td>{{ $sale->title }}</td>
                        <td>{{ $sale->price }}</td>
                        <td>{{ $sale->quantity }}</td>
                        <td>{{ $sale->latest_order }}</td>
                        <td>{{ $sale->name }}</td>
                        <td>{{ $sale->phone_number }}</td>
                        <td>{{ $sale->email }}</td>
                    </tr>
                @endforeach

                </tbody>
            </table>
        </div>

    </div>
@endsection