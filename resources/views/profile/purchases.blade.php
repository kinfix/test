@extends('layouts.base')

@section('content')

    <div class="container mt-5">

        <h1 class="text-center">Покупки</h1>

        <div class="col-12 mt-5">

            <table class="table table-hover">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Название</th>
                    <th scope="col">Цена(руб.)</th>
                    <th scope="col">Количество</th>
                    <th scope="col">Последняя продажа</th>
                    <th scope="col">Продавец</th>
                    <th scope="col">Телефон</th>
                    <th scope="col">Email</th>
                </tr>
                </thead>
                <tbody>
                @foreach( $purchases as $purchase)
                    <tr>
                        <td>{{ $purchase->title }}</td>
                        <td>{{ $purchase->price}}</td>
                        <td>{{ $purchase->quantity }}</td>
                        <td>{{ $purchase->latest_order }}</td>
                        <td>{{ $purchase->name }}</td>
                        <td>{{ $purchase->phone_number }}</td>
                        <td>{{ $purchase->email }}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>

    </div>
@endsection