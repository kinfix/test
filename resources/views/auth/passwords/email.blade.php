@extends('layouts.base')

@section('content')
    <div class="container mt-5">



        <div class="container mt-5">
            <form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
                {{ csrf_field() }}

                @if (session('status'))
                    <div class="alert alert-success">
                        {{ session('status') }}
                    </div>
                @endif

                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6 text-center">
                        <h2>Reset password</h2>
                        <hr>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-3 field-label-responsive">
                        <label for="email"></label>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                                <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-at"></i></div>
                                <input type="email" name="email" value="{{ old('email') }}"
                                       class="form-control" id="email"
                                       placeholder="Email" required autofocus>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-control-feedback">
                        <span class="text-danger align-middle">
                            <!-- Put e-mail validation error messages here -->
                            @if($errors->has('email'))
                                <i class="fa fa-close">{{ $errors->first('email') }}</i>
                            @endif
                        </span>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-6">
                        <button type="submit" class="btn btn-success fa fa-paper-plane">
                            Send password reset link.
                        </button>

                    </div>
                </div>
            </form>
        </div>
    </div>


@endsection
