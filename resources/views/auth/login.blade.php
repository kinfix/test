@extends('layouts.base')

@section('content')
    <div class="container mt-5">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('login') }}">
            {{ csrf_field() }}

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 text-center">
                    <h2>Вход</h2>
                    <hr>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 field-label-responsive">
                    <label for="email"></label>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-at"></i></div>
                            <input type="email" name="email" value="{{ old('email') }}"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"
                                   id="email"
                                   placeholder="Email" required autofocus>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-control-feedback">
                        <span class="text-danger align-middle">
                            <!-- Put e-mail validation error messages here -->
                            @if($errors->has('email'))
                                <i class="fa fa-close">{{ $errors->first('email') }}</i>
                            @endif
                        </span>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-md-3 field-label-responsive">
                    <label for="password"></label>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-key"></i></div>
                            <input type="password" name="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}"
                                   id="password"
                                   placeholder="Пароль" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-control-feedback">
                        <span class="text-danger align-middle">
                            @if ($errors->has('password'))
                                <!-- Put e-mail validation error messages here -->
                                    <i class="fa fa-close"> {{ $errors->first('password') }}</i>
                                @endif
                        </span>
                    </div>
                </div>
            </div>

            <div class="row mb-2">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <div class="checkbox">
                        <label>
                            <input type="checkbox"
                                   name="remember" {{ old('remember') ? 'checked' : '' }}> Запомнить меня
                        </label>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success fa fa-sign-in">
                        Войти
                    </button>

                    <a class="btn btn-link" href="{{ route('password.request') }}">
                        Забыли пароль?
                    </a>
                </div>
            </div>

        </form>

    </div>
@endsection
