@extends('layouts.base')

@section('content')
    <div class="container mt-5">
        <form class="form-horizontal" role="form" method="POST" action="{{ route('register') }}">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6 text-center">
                    <h2>Регистрация</h2>
                    <hr>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 field-label-responsive">
                    <label for="name"></label>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-user"></i></div>
                            <input type="text" name="name"
                                   class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" id="name"
                                   placeholder="Ваше имя" value="{{ old('name') }}" required autofocus>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-control-feedback">
                        <span class="text-danger align-middle">
                            <!-- Put name validation error messages here -->
                            @if ($errors->has('name'))
                                <i class="fa fa-close">{{ $errors->first('name') }}</i>
                            @endif
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 field-label-responsive">
                    <label for="email"></label>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-at"></i></div>
                            <input type="email" name="email" value="{{ old('email') }}"
                                   class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" id="email"
                                   placeholder="Email" required autofocus>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-control-feedback">
                        <span class="text-danger align-middle">
                            <!-- Put e-mail validation error messages here -->
                            @if($errors->has('email'))
                                <i class="fa fa-close">{{ $errors->first('email') }}</i>
                            @endif
                        </span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 field-label-responsive">
                    <label for="phone_number"></label>
                </div>
                <div class="col-md-6">
                    <div class="form-group{{ $errors->has('phone_number') ? ' has-error' : '' }}">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-phone"></i></div>
                            <input type="text" name="phone_number" value="{{ old('phone_number') }}"
                                   class="form-control{{ $errors->has('phone_number') ? ' is-invalid' : '' }}" id="phone_number"
                                   placeholder="+375xxxxxxxxx" required autofocus>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-control-feedback">
                        <span class="text-danger align-middle">
                            <!-- Put e-mail validation error messages here -->
                            @if($errors->has('phone_number'))
                                <i class="fa fa-close">{{ $errors->first('phone_number') }}</i>
                            @endif
                        </span>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-3 field-label-responsive">
                    <label for="password"></label>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">
                            <div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-key"></i></div>
                            <input type="password" name="password"
                                   class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" id="password"
                                   placeholder="Пароль" required>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-control-feedback">
                        <span class="text-danger align-middle">
                            @if ($errors->has('password'))
                                <!-- Put e-mail validation error messages here -->
                                    <i class="fa fa-close"> {{ $errors->first('password') }}</i>
                                @endif
                        </span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 field-label-responsive">
                    <label for="password_confirmation"></label>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <div class="input-group mb-2 mr-sm-2 mb-sm-0">

                            <div class="input-group-addon" style="width: 2.6rem">
                                <i class="fa fa-repeat"></i>
                            </div>

                            <input type="password" name="password_confirmation" class="form-control"
                                   id="password_confirmation" placeholder="Подтвержденние пароля" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <button type="submit" class="btn btn-success"><i class="fa fa-user-plus"></i> Регистрация</button>
                </div>
            </div>
        </form>
    </div>
@endsection
