@extends('layouts.base')

@section('content')
    <div class="container mt-5">
        <div class="modal-body">
            <div class="row">
                <div class="col-md-6 product_img">
                    <img src="{{ asset('storage/' . $product->image) }}" width="400" height="400">
                </div>
                <div class="col-md-6 product_content">
                    <h4>{{ $product->title }}</h4>

                    <p class="mb-5">{{ $product->description }}</p>

                    <h3><span class="mt-5"></span>

                        {{ $product->price }} руб.

                    </h3>

                    <p>Количество: {{ $product->quantity }}</p>
                    <div class="space-ten"></div>
                    <div class="btn-ground">

                        @isset(Auth::user()->id)
                            @if($product->user_id == Auth::user()->id)
                                <a href="/editor/{{ $product->id }}" class="btn btn-secondary">Редактировать</a>
                            @elseif($product->quantity >0)
                                <a href="/order/{{ $product->id }}" type="button" class="btn btn-primary">Купить</a>
                            @endif
                        @endisset

                        @if(Session::has('message'))
                            <h4 class="mt-5 text-success">{{ Session::get('message') }}</h4>
                        @elseif(Session::has('updated'))
                            <h4 class="mt-5 text-success">{{ Session::get('updated') }}</h4>
                        @endif
                    </div>

                    <div class="mt-5">
                        @foreach($sellerInfo as $seller)
                            <h4>Продавец:</h4>
                            <p>Имя: {{ $seller->name }}</p>
                            <p>Email: {{ $seller->email }}</p>
                            <p>Телефон: {{ $seller->phone_number }}</p>
                        @endforeach
                    </div>

                </div>


            </div>
        </div>
    </div>

@endsection