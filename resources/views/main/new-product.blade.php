@extends('layouts.base')

@section('content')
    <div class="container mt-5 d-flex justify-content-center">
        <div class="col-md-8">
            <h4 class="mb-3">Добавить товар</h4>
            <form class="needs-validation" method="POST" enctype="multipart/form-data" action="/store-product">
                {{ csrf_field() }}
                <div class="mb-3">
                    <label for="username">Название товара</label>
                    <div class="input-group">
                        <input type="text" class="form-control" id="username" name="title" placeholder="Название" maxlength="100"
                               required>
                        <div class="invalid-feedback" style="width: 100%;">
                            Your username is required.
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6 mb-3">
                        <label for="firstName">Цена</label>
                        <input type="text" class="form-control" id="firstName" name="price" value="" required>
                        <div class="invalid-feedback">
                            Valid first name is required.
                        </div>
                    </div>
                    <div class="col-md-6 mb-3">
                        <label for="lastName">Количество (до 100 единиц)</label>
                        <input type="text" class="form-control" id="lastName" name="quantity" value="" required>
                        <div class="invalid-feedback">
                            Valid last name is required.
                        </div>
                    </div>
                </div>

                <div class="mb-3">
                    <label for="username">Описание товара</label>
                    <div class="input-group">
                        <textarea type="text" class="form-control" maxlength="64000" id="username" name="description"
                                  placeholder="Описание" required></textarea>
                        <div class="invalid-feedback" style="width: 100%;">

                        </div>
                    </div>
                </div>

                <div class="row mt-4">

                    <div class="col-md-6">
                        <p>Загрузите изображение товара (до 2Mb) -></p>

                    </div>
                    <div class="col-md-6">

                        <input type="file" name="image"/>
                    </div>
                </div>

                <input type="hidden" name="user_id" value="{{ Auth::user()->id }}">

                <hr class="mb-4">
                <button class="btn btn-primary btn-lg btn-block" type="submit">Добавить</button>
            </form>
        </div>
    </div>
@endsection