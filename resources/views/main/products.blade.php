@extends('layouts.base')

@section('content')

    <main role="main">
        <div class="album py-5 ">
            <div class="container">

                <div class="row">
                    <form action="/" method="GET">
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <label class="input-group-text" for="inputGroupSelect01">Сортировать по</label>
                            </div>
                            <select class="custom-select col-12" id="inputGroupSelect01" name="filter">
                                <option selected value="created_at">Дата</option>
                                <option value="title">Название</option>
                                <option value="price">Цена</option>
                            </select>
                            <div class="input-group-prepend">
                                <button class="btn btn-primary input-group-btn" type="submit">Ок</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="row">
                    @foreach( $products as $product)
                        <div class="col-md-4">
                            <div class="card mb-4 box-shadow">
                                <a href="/product/{{ $product->id }}/">
                                    <img class="card-img-top" height="300" width="300"
                                         src="{{ asset('storage/' . $product->image) }}"
                                         alt="Card image cap">
                                </a>
                                <div class="card-body">

                                    <p class="card-text">
                                        <a href="/product/{{ $product->id }}/">{{ $product->title }}</a>
                                    </p>

                                    <div class="d-flex justify-content-between align-items-center">
                                        <div class="btn-group">
                                            {{--<button type="button" class="btn btn-sm btn-outline-secondary">View</button>--}}
                                            @isset(Auth::user()->id)
                                                @if($product->user_id == Auth::user()->id)
                                                    <button type="button"
                                                            class="btn btn-sm btn-outline-success disabled">Ваш лот
                                                    </button>
                                                @endif
                                            @endisset
                                        </div>
                                        <small class="text-muted">{{ $product->price }} руб.</small>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>

            </div>
        </div>

    </main>
@endsection