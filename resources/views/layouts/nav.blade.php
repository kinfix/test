<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container">
        <a class="navbar-brand">Testo</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">На главную <span class="sr-only">(current)</span></a>
                </li>
            </ul>


            @guest
                <div class="nav-item btn">
                    <a href="{{ route('register') }}" class="ui primary basic button">Регистрация</a>
                </div>
                <div class="nav-item btn">
                    <a href="{{ route('login') }}" class="ui primary button">Вход</a>
                </div>
            @else
                <a href="/new-product" class="mr-auto btn btn-primary center-block">
                    + Добавить товар
                </a>
                <div class="nav-item dropdown">
                    <a class="dropdown-toggle" href="#" id="navbarDropdown" role="button"
                       data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        {{ Auth::user()->name }}
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="/purchases">Мои покупки</a>
                        <a class="dropdown-item" href="/sales">Мои продажи</a>
                        <div class="dropdown-divider"></div>
                        <a class="dropdown-item" href="{{ route('logout') }}"
                           onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                            Выйти
                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            {{ csrf_field() }}
                            <button type="submit" class="item">Выйти</button>
                        </form>
                    </div>
                    @endguest
                </div>
        </div>
    </div>
</nav>

{{--Add product--}}


<!-- line modal -->
{{--<div class="modal fade" id="squarespaceModal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"--}}
{{--aria-hidden="true">--}}
{{--<div class="modal-dialog">--}}
{{--<div class="modal-content">--}}
{{--<div class="modal-header">--}}
{{--<h3>Добавить товар</h3>--}}
{{--<div class="btn-group btn-group-justified" role="group" aria-label="group button">--}}
{{--<div class="btn-group" role="group">--}}
{{--<button type="button" class="btn btn-default" data-dismiss="modal" role="button">Close</button>--}}
{{--</div>--}}

{{--</div>--}}
{{--</div>--}}
{{--<div class="modal-body">--}}

{{--<!-- content goes here -->--}}
{{--<form action="" method="POST" enctype="multipart/form-data">--}}
{{--<div class="form-group">--}}

{{--<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Название товара">--}}
{{--</div>--}}
{{--<div class="form-row">--}}
{{--<div class="col">--}}
{{--<input type="text" class="form-control" placeholder="Цена">--}}
{{--</div>--}}
{{--<div class="col">--}}
{{--<input type="text" class="form-control" placeholder="Количество( до 100 штук)">--}}
{{--</div>--}}
{{--</div>--}}

{{--<div class="form-group">--}}
{{--<textarea  class="form-control mt-3" id="exampleInputEmail1"--}}
{{--maxlength="65000" placeholder="Введите описание товара"></textarea>--}}
{{--</div>--}}

{{--<div class="form-group">--}}
{{--<label for="exampleInputFile">Загрузите фото товара</label>--}}
{{--<input type="file" id="exampleInputFile">--}}
{{--<p class="help-block"></p>--}}
{{--</div>--}}

{{--<button type="submit" class="btn btn-default">Добавить</button>--}}
{{--</form>--}}

{{--</div>--}}

{{--</div>--}}
{{--</div>--}}
{{--</div>--}}